import React, { useState } from "react";
import { Team } from "../../Assets/file";
import LineUpModal from "../../Components/Modal/Block/LineUpModal";
import { OverFlowX } from "../../Styles/PicksterBoard";
import { ProtoFlexBox, ProtoImg, ProtoTeamName } from "../../Styles/Proto";
import {
  PurchaseContainer,
  PurchaseTitle,
  PurchaseTop_tbody,
  PurchaseTop_td,
  PurchaseTop_th,
  PurchaseTop_thead,
  PurchaseWrapper,
  Purchase_Table,
} from "../../Styles/PurchaseRate";

import CustomModal from "../../Components/Modal/index";

const LineUp = () => {
  const [data, setData] = useState([{}, {}]);
  const [isOpen, setIsOpen] = useState(false);
  return (
    <>
      <CustomModal
        isOpen={isOpen}
        setIsOpen={setIsOpen}
        Content={LineUpModal}
      />
      <PurchaseWrapper>
        <PurchaseContainer>
          <PurchaseTitle title>라인업</PurchaseTitle>
          <OverFlowX>
            <Purchase_Table>
              <PurchaseTop_thead>
                <PurchaseTop_th style={{ width: 125 }}>경기</PurchaseTop_th>
                <PurchaseTop_th style={{ width: 175 }}>홈팀</PurchaseTop_th>
                <PurchaseTop_th style={{ width: 95 }}></PurchaseTop_th>
                <PurchaseTop_th style={{ width: 225 }}>원정팀</PurchaseTop_th>
                <PurchaseTop_th style={{ width: 100 }}>O</PurchaseTop_th>
                <PurchaseTop_th style={{ width: 100 }}>OO</PurchaseTop_th>
                <PurchaseTop_th style={{ width: 100 }}>OA</PurchaseTop_th>
              </PurchaseTop_thead>
              {data?.map((el, idx) => {
                return (
                  <PurchaseTop_tbody
                    lineup
                    onClick={() => {
                      setIsOpen(true);
                    }}
                  >
                    <PurchaseTop_td style={{ width: 125 }}>
                      JPND2
                    </PurchaseTop_td>
                    <PurchaseTop_td style={{ width: 175 }}>
                      <ProtoFlexBox>
                        <ProtoTeamName>레알마드리드</ProtoTeamName>
                        <ProtoImg src={Team}></ProtoImg>
                      </ProtoFlexBox>
                    </PurchaseTop_td>
                    <PurchaseTop_td style={{ width: 95 }}>vs</PurchaseTop_td>
                    <PurchaseTop_td style={{ width: 225 }}>
                      <ProtoFlexBox>
                        <ProtoImg src={Team}></ProtoImg>
                        <ProtoTeamName>레알마드리드</ProtoTeamName>
                      </ProtoFlexBox>
                    </PurchaseTop_td>
                    <PurchaseTop_td style={{ width: 100 }}>11</PurchaseTop_td>
                    <PurchaseTop_td style={{ width: 100 }}>11</PurchaseTop_td>
                    <PurchaseTop_td style={{ width: 100 }}>11</PurchaseTop_td>
                  </PurchaseTop_tbody>
                );
              })}
            </Purchase_Table>
          </OverFlowX>
        </PurchaseContainer>
      </PurchaseWrapper>
    </>
  );
};

export default LineUp;

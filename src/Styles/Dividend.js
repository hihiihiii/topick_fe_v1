import styled from "@emotion/styled";

const breakpoints = [375, 768, 1200, 1440];

const mq = breakpoints.map((bp) => `@media (max-width : ${bp}px)`);

const DividenedFlexBox = styled.div`
  width: 100%;
  display: flex;
  align-items: center;
  justify-content: space-between;
  margin-bottom: 10px;
`;

const DropTimeBox = styled.div`
  width: 136px;
  height: 45px;
  background-color: #b068f8;
  color: #fff;
  font-size: 14px;
  font-weight: 500;
  display: flex;
  align-items: center;
  justify-content: center;
  border-radius: 10px;
  cursor: pointer;

  ${mq[1]} {
    width: 116px;
    font-size: 12px;
  }
  ${mq[0]} {
    width: 110px;
  }
`;

const DropBox = styled.div`
  width: 131.1px;
  height: 51.8px;
  display: flex;
  align-items: center;
  background-color: ${(props) => {
    if (props.red) {
      return "#ffdede";
    }
    if (props.blue) {
      return "#dee7ff";
    }
    if (props.black) {
      return "#ededed";
    }
  }};
  box-sizing: border-box;
  border-radius: 8px;
  padding: 10px;
  margin-right: 8px;
`;

const DropFlex = styled.div`
  display: flex;
  flex-direction: ${(props) => {
    if (props.row) {
      return "row";
    }
    return "column";
  }};
  align-items: ${(props) => {
    if (props.row) {
      return "center";
    }
  }};
`;

const DropImg = styled.img`
  width: ${(props) => {
    if (props.mini) {
      return "10.4px";
    }
    return "14px";
  }};
  height: ${(props) => {
    if (props.mini) {
      return "4.2px";
    }
    return "14px";
  }};
  object-fit: contain;

  /* width: 10.4px;
  height: 4.2px; */
`;

const DropText = styled.div`
  font-size: ${(props) => {
    if (props.bottom) {
      return "9px";
    }
    return "14px";
  }};
  color: #000;
  font-weight: ${(props) => {
    if (props.bottom) {
      return "300";
    }
    return "600";
  }};
`;

export { DividenedFlexBox, DropTimeBox, DropBox, DropFlex, DropImg, DropText };

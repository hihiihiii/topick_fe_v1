import styled from "@emotion/styled";

const breakpoint = [450, 768, 1200, 1920];
const mq = breakpoint.map((bp) => `@media (max-width: ${bp}px)`);

const HeaderBox = styled.div`
  width: 110px;
  height: 40px;
  display: flex;
  align-items: center;
  justify-content: center;
  color: #fff;
  font-size: 13px;
  font-weight: 600;
  border-radius: 4px;
  background-color: #b975fd;
  cursor: pointer;

  ${mq[0]} {
    width: 90px;
    height: 32px;
    font-size: 10px;
  }
`;

const PayMiddleBox = styled.div`
  width: 621px;
  height: 100%;
  padding: 25px;
  border-radius: 4px;
  box-shadow: 0 3px 6px 0 rgba(0, 0, 0, 0.16);
  background-color: #fff;
  position: relative;
  box-sizing: border-box;
  margin-top: 20px;
  ${mq[0]} {
    width: 100%;
    padding: 20px;
    height: 100%;
  }
`;

const PayOnline = styled.div`
  width: 8px;
  height: 8px;
  border-radius: 10px;
  background-color: #b975fd;
  top: 12px;
  left: 12px;
  position: absolute;

  ${mq[0]} {
    top: 8px;
    left: 8px;
  }
`;

const PayText = styled.div`
  font-size: ${(props) => {
    if (props.sub) {
      return "14px";
    }
    if (props.bottom) {
      return "16px";
    }
    return "17px";
  }};
  font-weight: ${(props) => {
    if (props.sub) {
      return "300";
    }
    if (props.bottom) {
      return "500";
    }
    return "600";
  }};
  color: #000;
  line-height: 25px;

  ${mq[0]} {
    font-size: ${(props) => {
      if (props.sub) {
        return "12px";
      }
      if (props.bottom) {
        return "16px";
      }
      return "15px";
    }};
    line-height: 15px;
    margin-top: ${(props) => {
      if (props.sub) {
        return "10px";
      }
      return "0px";
    }};
  }
`;

const PayContentBox = styled.div`
  width: 100%;
  display: flex;
  flex-direction: column;
  ${mq[0]} {
    width: 100%;
  }
`;

const PayBottomBox = styled.div`
  display: flex;
  flex-direction: column;
  margin-top: 50px;
`;

const PayWrpper = styled.div`
  margin-top: 20px;
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
`;

const PayBox = styled.div`
  padding: 25px;
  width: 308px;
  height: 180px;
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  border-radius: 7px;
  box-shadow: 0 3px 6px 0 rgba(0, 0, 0, 0.16);
  background-color: #fff;
  box-sizing: border-box;
  margin: 0px 10px 10px 0px;

  ${mq[1]} {
    margin: 0px 7.5px 7.5px 0px;
  }
  ${mq[0]} {
    margin-right: 0px;
    width: 100%;
  }
`;

const PayInnerText = styled.div`
  font-size: ${(props) => {
    if (props.bottom) {
      return "14px";
    }
    if (props.title) {
      return "22px";
    }
    return "16px";
  }};
  font-weight: ${(props) => {
    if (props.bottom) {
      return "300";
    }
    return "600";
  }};
  color: ${(props) => {
    if (props.title) {
      return "#000";
    }
    return "#9d9d9d";
  }};

  ${mq[0]} {
    font-size: ${(props) => {
      if (props.bottom) {
        return "12px";
      }
      if (props.title) {
        return "20px";
      }
      return "16px";
    }};
  }
`;

const PayInnerRow = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
`;

export {
  PayInnerRow,
  PayWrpper,
  PayBox,
  PayInnerText,
  HeaderBox,
  PayMiddleBox,
  PayOnline,
  PayText,
  PayContentBox,
  PayBottomBox,
};

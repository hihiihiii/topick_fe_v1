import React from "react";
import {
  BoldText,
  CancleBtn,
  CardText,
  GameText,
  GameTextFlex,
  GameTitleBox,
  InfoTable,
  InfoTableText,
  InfoTbody,
  InfoThead,
  InfoWrapper,
  OddsBox,
  RelatedOverFlow,
} from "../../../Styles/Modal/Infomation";

const TeamInfomation = ({ setIsOpen }) => {
  return (
    <>
      <InfoWrapper>
        <GameTitleBox>
          <CardText>레알 마드리드 - FC Real Madrid</CardText>
          <CancleBtn
            onClick={() => {
              setIsOpen(false);
            }}
          ></CancleBtn>
        </GameTitleBox>

        <GameTextFlex>
          <GameText>
            총 <BoldText>20</BoldText>경기
          </GameText>
          <GameText>9승 1무 10패</GameText>
        </GameTextFlex>
        <GameTextFlex>
          <GameText>
            총 <BoldText>10</BoldText>경기 핸디캡 적용
          </GameText>
          <GameText>9승 1무 10패</GameText>
        </GameTextFlex>
        <GameTextFlex>
          <GameText>
            총 <BoldText bold>20</BoldText>경기 오버
          </GameText>
          <GameText>
            총 <BoldText>10</BoldText>경기 언더
          </GameText>
          <GameText>
            총 <BoldText>10</BoldText> 경기 짝
          </GameText>
        </GameTextFlex>

        <RelatedOverFlow>
          <InfoTable>
            <InfoThead>
              <InfoTableText style={{ width: 120 }} th>
                경기
              </InfoTableText>
              <InfoTableText style={{ width: 120 }} th>
                시간
              </InfoTableText>
              <InfoTableText style={{ width: 150 }} th>
                홈팀
              </InfoTableText>
              <InfoTableText style={{ width: 150 }} th>
                결과
              </InfoTableText>
              <InfoTableText style={{ width: 150 }} th>
                원정팀
              </InfoTableText>
              <InfoTableText style={{ width: 150 }} th>
                승률
              </InfoTableText>
              <InfoTableText style={{ width: 150 }} th>
                홀짝
              </InfoTableText>
            </InfoThead>
            <InfoTbody>
              <InfoTableText style={{ width: 120 }}>JPND2</InfoTableText>
              <InfoTableText style={{ width: 120 }}>2021.09.01</InfoTableText>
              <InfoTableText style={{ width: 150 }}>레알마드리드</InfoTableText>
              <InfoTableText style={{ width: 150 }}>3:2</InfoTableText>
              <InfoTableText style={{ width: 150 }}>레알마드리드</InfoTableText>
              <InfoTableText style={{ width: 150 }}>
                <OddsBox>패배</OddsBox>
              </InfoTableText>
              <InfoTableText style={{ width: 150 }}>홀</InfoTableText>
            </InfoTbody>
          </InfoTable>
        </RelatedOverFlow>

        <RelatedOverFlow>
          <InfoTable>
            <InfoThead>
              <InfoTableText style={{ width: 150 }} th>
                승 (합계)
              </InfoTableText>
              <InfoTableText style={{ width: 150 }} th>
                무 (합계)
              </InfoTableText>
              <InfoTableText style={{ width: 150 }} th>
                패 (합계)
              </InfoTableText>
              <InfoTableText style={{ width: 150 }} th>
                홀 승
              </InfoTableText>
              <InfoTableText style={{ width: 150 }} th>
                홀 무
              </InfoTableText>
              <InfoTableText style={{ width: 150 }} th>
                홀 패
              </InfoTableText>
              <InfoTableText style={{ width: 150 }} th>
                중립 승
              </InfoTableText>
              <InfoTableText style={{ width: 150 }} th>
                중립 무
              </InfoTableText>
              <InfoTableText style={{ width: 150 }} th>
                중립 패
              </InfoTableText>
              <InfoTableText style={{ width: 150 }} th>
                원정 승
              </InfoTableText>
              <InfoTableText style={{ width: 150 }} th>
                원정 무
              </InfoTableText>
              <InfoTableText style={{ width: 150 }} th>
                원정 패
              </InfoTableText>
            </InfoThead>
            <InfoTbody>
              <InfoTableText style={{ width: 120 }}>2</InfoTableText>
              <InfoTableText style={{ width: 120 }}>2</InfoTableText>
              <InfoTableText style={{ width: 120 }}>2</InfoTableText>
              <InfoTableText style={{ width: 120 }}>2</InfoTableText>
              <InfoTableText style={{ width: 120 }}>2</InfoTableText>
              <InfoTableText style={{ width: 120 }}>2</InfoTableText>
              <InfoTableText style={{ width: 120 }}>2</InfoTableText>
              <InfoTableText style={{ width: 120 }}>2</InfoTableText>
              <InfoTableText style={{ width: 120 }}>2</InfoTableText>
              <InfoTableText style={{ width: 120 }}>2</InfoTableText>
              <InfoTableText style={{ width: 120 }}>2</InfoTableText>
              <InfoTableText style={{ width: 120 }}>2</InfoTableText>
            </InfoTbody>
            <InfoTbody>
              <InfoTableText style={{ width: 120 }}>45.00%</InfoTableText>
              <InfoTableText style={{ width: 120 }}>45.00%</InfoTableText>
              <InfoTableText style={{ width: 120 }}>45.00%</InfoTableText>
              <InfoTableText style={{ width: 120 }}>45.00%</InfoTableText>
              <InfoTableText style={{ width: 120 }}>45.00%</InfoTableText>
              <InfoTableText style={{ width: 120 }}>45.00%</InfoTableText>
              <InfoTableText style={{ width: 120 }}>45.00%</InfoTableText>
              <InfoTableText style={{ width: 120 }}>45.00%</InfoTableText>
              <InfoTableText style={{ width: 120 }}>45.00%</InfoTableText>
              <InfoTableText style={{ width: 120 }}>45.00%</InfoTableText>
              <InfoTableText style={{ width: 120 }}>45.00%</InfoTableText>
              <InfoTableText style={{ width: 120 }}>45.00%</InfoTableText>
            </InfoTbody>
          </InfoTable>
        </RelatedOverFlow>
      </InfoWrapper>
    </>
  );
};
export default TeamInfomation;

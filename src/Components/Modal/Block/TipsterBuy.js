import React from "react";
import { Tipster } from "../../../Assets/file";
import {
  ModalText,
  TipsterBar,
  TipsterBtn,
  TipsterFlex,
  TipsterImg,
  TipsterMiddleBox,
  TipsterWrapper,
} from "../../../Styles/Modal/Modal";
import { Linked } from "../../../Styles/Side";

const TipsterBuy = ({ setIsOpen }) => {
  return (
    <>
      <TipsterWrapper buy>
        <ModalText title>픽스터 팁 구매하기</ModalText>
        <TipsterMiddleBox>
          <TipsterImg url={Tipster}></TipsterImg>
          <ModalText>레알마드리드 vs 레알마드리드</ModalText>
        </TipsterMiddleBox>
        <TipsterFlex>
          <ModalText>보유포인트</ModalText>
          <ModalText>15,000 P</ModalText>
        </TipsterFlex>
        <TipsterFlex>
          <ModalText>픽스터 팁 가격</ModalText>
          <ModalText>15,000 P</ModalText>
        </TipsterFlex>
        <TipsterBar />
        <TipsterFlex>
          <ModalText>결제 후 포인트</ModalText>
          <ModalText>15,000 P</ModalText>
        </TipsterFlex>

        <TipsterFlex>
          <TipsterBtn
            onClick={() => {
              setIsOpen(false);
            }}
            cancle
          >
            취소
          </TipsterBtn>
          <Linked more to="/pick/paiddetail">
            <TipsterBtn>완료</TipsterBtn>
          </Linked>
        </TipsterFlex>
      </TipsterWrapper>
    </>
  );
};

export default TipsterBuy;

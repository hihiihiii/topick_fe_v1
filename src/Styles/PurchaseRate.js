import styled from "@emotion/styled";

const breakpoints = [450, 768, 1200];
const mq = breakpoints.map((bp) => `@media (max-width : ${bp}px)`);

const PurchaseWrapper = styled.div`
  margin: 0px auto;
  padding: 0px 140px;

  ${mq[1]} {
    padding: 0px 67.2px;
  }
  ${mq[0]} {
    padding: 0px 27.2px;
  }
`;
const PurchaseContainer = styled.div`
  margin-top: 44px;

  ${mq[1]} {
    width: 100%;
  }
  ${mq[0]} {
    width: 100%;
  }
`;

const PurchaseTitle = styled.div`
  width: 101px;
  height: 28px;
  font-size: 22px;
  font-weight: 600;
  color: #000;
  margin-bottom: ${(props) => {
    if (props.title) {
      return "20px";
    }
    if (props.rank) {
      return "10px";
    }
    return "0px";
  }};

  ${mq[0]} {
    font-size: 18px;
  }
`;

const Purchase_Table = styled.div`
  width: 100%;
  ${mq[1]} {
    width: fit-content;
  }
  ${mq[0]} {
    width: fit-content;
  }
`;

const PurchaseTop_thead = styled.div`
  width: 100%;
  height: 57px;
  padding: 0px 50px;
  box-sizing: border-box;
  background-color: #f2e5ff;
  display: flex;
  align-items: center;

  border-top-left-radius: 10px;
  border-top-right-radius: 10px;
`;

const PurchaseTop_tbody = styled.div`
  display: flex;
  align-items: center;
  box-sizing: border-box;
  padding: 0px 50px;
  height: ${(props) => {
    if (props.covers) {
      return "85px";
    }
    return "60px";
  }};
  cursor: ${(props) => {
    if (props.lineup) {
      return "pointer";
    }
  }};
  :hover {
    background-color: ${(props) => {
      if (props.lineup) {
        return "#f8f8f8";
      }
    }};
  }
`;

const PurchaseTop_th = styled.div`
  font-size: 13px;
  font-weight: 600;
  color: #888;
`;

const PurchaseTop_td = styled.div`
  font-size: 14px;
  font-weight: 600;
  color: #000;
  text-decoration: ${(props) => {
    if (props.pointer) {
      return "underline";
    }
  }};
  cursor: ${(props) => {
    if (props.pointer) {
      return "pointer";
    }
  }};
`;

const ResultBox = styled.div`
  width: 91px;
  height: 47px;
  padding: 14px;
  box-sizing: border-box;
  background-color: #f8f8f8;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  border-radius: 10px;
`;

const ResultText = styled.div`
  font-size: ${(props) => {
    if (props.small) {
      return "12px";
    }
    return "15px";
  }};

  color: #000;
  text-align: center;
`;

export {
  ResultBox,
  ResultText,
  PurchaseWrapper,
  PurchaseContainer,
  PurchaseTop_thead,
  PurchaseTitle,
  Purchase_Table,
  PurchaseTop_tbody,
  PurchaseTop_th,
  PurchaseTop_td,
};

import styled from "@emotion/styled";

const breakpoint = [450, 768, 1200, 1920];
const mq = breakpoint.map((bp) => `@media (max-width: ${bp}px)`);

const BlogContainer = styled.div`
  display: flex;
  flex-direction: column;
`;

const BlogTop = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  width: 100%;
  margin-top: 20px;
  margin-bottom: 10px;

  ${mq[0]} {
    width: fit-content;
  }
`;

const BlogTopBox = styled.div`
  width: 200px;
  height: 165px;
  box-shadow: 0 3px 6px 0 rgba(0, 0, 0, 0.16);
  border-radius: 7px;

  ${mq[0]} {
    width: 140px;
    margin-right: 16px;
  }
`;

const BlogInner = styled.div`
  display: flex;
  flex-direction: column;
  padding: 25px;
  box-sizing: border-box;
`;

const BlogText = styled.div`
  font-size: ${(props) => {
    if (props.sub) {
      return "12px";
    }
    return "18px";
  }};
  font-weight: ${(props) => {
    if (props.sub) {
      return "500";
    }
    return "600";
  }};
  color: ${(props) => {
    if (props.sub) {
      return "#a7a7a7";
    }
    return "#000";
  }};
  margin-top: ${(props) => {
    if (props.sub) {
      return "25px";
    }
  }};
`;

const BlogCircle = styled.div`
  width: 50px;
  height: 50px;
  border-radius: 999px;
  background-color: ${(props) => {
    if (props.purple) {
      return "#ededff";
    }
    if (props.green) {
      return "#eeffed";
    }
    if (props.yellow) {
      return "#fff9ed";
    }
  }};
`;

const BlogMiddleBox = styled.div`
  margin-top: 50px;
`;

const BlogChartBox = styled.div`
  width: 100%;
  height: 342px;
  border-radius: 7px;
  margin-top: 15px;
  box-shadow: 0 3px 6px 0 rgba(0, 0, 0, 0.16);
  background-color: #fff;

  ${mq[0]} {
    width: 100%;
  }
`;

const BlogModi = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  width: 100%;
  margin-top: 50px;
`;

const BlogModiText = styled.div`
  font-size: 14px;
  font-weight: 600;
  text-decoration: underline;
  color: #0d55de;
  cursor: pointer;
`;

const BlogContent = styled.div`
  width: 100%;
  font-size: 16px;
  font-weight: 500;
  line-height: 1.5;
  color: #646464;
  margin-top: 30px;
`;

const BlogBottom = styled.div`
  margin-top: 69.5px;
`;

const BlogTable = styled.div`
  margin-top: 20px;
  width: 100%;
  border-radius: 7px;
  ${mq[0]} {
    width: fit-content;
  }
`;
const BlogThead = styled.div`
  width: 100%;
  background-color: #f5f5f5;
  padding: 0px 70px;
  box-sizing: border-box;
  height: 60px;
  display: flex;
  align-items: center;
  border-top-left-radius: 10px;
  border-top-right-radius: 10px;
`;
const BlogTh = styled.div`
  font-size: 12px;
  font-weight: 600;
  color: #a7a7a7;
`;
const BlogTbody = styled.div`
  width: 100%;
  height: 60px;
  display: flex;
  align-items: center;
  padding: 0px 70px;
  box-sizing: border-box;
  border-bottom: 1px solid #ededed;
`;
const BlogTd = styled.div`
  font-size: 14px;
  font-weight: 600;
  color: #000;
`;

const BlogOverScroll = styled.div`
  ${mq[0]} {
    overflow-x: scroll;
    height: 100%;
    ::-webkit-scrollbar {
      display: none;
    }
  }
`;

export {
  BlogOverScroll,
  BlogTable,
  BlogThead,
  BlogTh,
  BlogTbody,
  BlogTd,
  BlogBottom,
  BlogContent,
  BlogModiText,
  BlogModi,
  BlogChartBox,
  BlogContainer,
  BlogTop,
  BlogTopBox,
  BlogInner,
  BlogText,
  BlogCircle,
  BlogMiddleBox,
};

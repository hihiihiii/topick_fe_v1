import React, { useState } from "react";
import { Team } from "../../Assets/file";
import { CoversFlexBox, CoversImg, CoversText } from "../../Styles/Covers";
import { OverFlowX } from "../../Styles/PicksterBoard";
import {
  PurchaseContainer,
  PurchaseTitle,
  PurchaseTop_tbody,
  PurchaseTop_td,
  PurchaseTop_th,
  PurchaseTop_thead,
  PurchaseWrapper,
  Purchase_Table,
} from "../../Styles/PurchaseRate";
import { RankBtnBox, RankFlexBox } from "../../Styles/Rank";

const Rank = () => {
  const [data, setData] = useState([{}, {}]);
  const [action, setAction] = useState(true);

  //   const _handleChecker = (idx) => {
  //     for (let i = 0; i < data.length; i++) {
  //       if (data[i].check) {
  //         data[i].check = false;
  //       }
  //     }
  //     const datas = [...data];
  //     datas[idx].check = !data[idx].check;
  //     setData(datas);
  //   };

  return (
    <>
      <PurchaseWrapper>
        <PurchaseContainer>
          <PurchaseTitle rank>픽스터 랭킹</PurchaseTitle>
          <RankFlexBox>
            <RankBtnBox
              action={action ? true : false}
              onClick={() => {
                setAction(true);
              }}
            >
              수익
            </RankBtnBox>
            <RankBtnBox
              action={action ? false : true}
              onClick={() => {
                setAction(false);
              }}
            >
              수익률
            </RankBtnBox>
          </RankFlexBox>
          <OverFlowX>
            <Purchase_Table>
              <PurchaseTop_thead>
                <PurchaseTop_th style={{ width: 125 }}>순위</PurchaseTop_th>
                <PurchaseTop_th style={{ width: 175 }}>닉네임</PurchaseTop_th>
                <PurchaseTop_th style={{ width: 200 }}>수익</PurchaseTop_th>
                <PurchaseTop_th style={{ width: 250 }}>
                  투자대비수익률
                </PurchaseTop_th>
                <PurchaseTop_th style={{ width: 100 }}>픽</PurchaseTop_th>
              </PurchaseTop_thead>
              {data?.map((el, idx) => {
                return (
                  <PurchaseTop_tbody>
                    <PurchaseTop_td style={{ width: 125 }}>
                      {idx + 1}
                    </PurchaseTop_td>
                    <PurchaseTop_td style={{ width: 175 }}>
                      <CoversFlexBox>
                        <CoversFlexBox row>
                          <CoversText>dlduddls</CoversText>
                        </CoversFlexBox>
                      </CoversFlexBox>
                    </PurchaseTop_td>
                    <PurchaseTop_td style={{ width: 200 }}>
                      <CoversFlexBox>
                        <CoversText>+1,000</CoversText>
                      </CoversFlexBox>
                    </PurchaseTop_td>
                    <PurchaseTop_td style={{ width: 250 }}>
                      <CoversFlexBox>
                        <CoversText title>122%</CoversText>
                      </CoversFlexBox>
                    </PurchaseTop_td>
                    <PurchaseTop_td style={{ width: 100 }}>
                      <CoversFlexBox>
                        <CoversText>122%</CoversText>
                      </CoversFlexBox>
                    </PurchaseTop_td>
                  </PurchaseTop_tbody>
                );
              })}
            </Purchase_Table>
          </OverFlowX>
        </PurchaseContainer>
      </PurchaseWrapper>
    </>
  );
};

export default Rank;

import styled from "@emotion/styled";

const CoversFlexBox = styled.div`
  width: 100%;
  display: flex;
  flex-direction: ${(props) => {
    if (props.row) {
      return "row";
    }
    return "column";
  }};
  align-items: ${(props) => {
    if (props.row) {
      return "center";
    }
    return "";
  }};
`;

const CoversText = styled.div`
  //height: 30px;
  font-size: ${(props) => {
    if (props.title) {
      return "18px";
    }
    return "15px";
  }};
  font-weight: 600;
  color: #000;
  margin-right: 10px;
  width: ${(props) => {
    if (props.date) {
      return "80px";
    }
  }};
`;

const CoversImg = styled.div`
  width: 17px;
  height: 22.8px;
  background-image: ${(props) => {
    if (props.url) {
      return `url(${props.url})`;
    }
  }};
  background-repeat: no-repeat;
  background-size: cover;

  // 실 데이터 들어가면 삭제해야함!!
  margin-bottom: 4px;
`;

export { CoversFlexBox, CoversText, CoversImg };

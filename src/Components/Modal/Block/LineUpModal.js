import React, { useState } from "react";
import { Country, LineUp, Team } from "../../../Assets/file";
import {
  Injury,
  LineContentsContainer,
  LineTbody,
  LineThead,
  LineTitleBox,
  LineUpBox,
  LineUpLogo,
  LineUpTeamBox,
  LineUpText,
  LineUpTitle,
  OverFlow,
  SquadImg,
} from "../../../Styles/Modal/LineUpModal";
import { OverFlowX } from "../../../Styles/PicksterBoard";

const LineUpModal = () => {
  const [data, setData] = useState([
    {},
    {},
    {},
    {},
    {},
    {},
    {},
    {},
    {},
    {},
    {},
  ]);
  return (
    <>
      <LineUpBox>
        <LineTitleBox>
          <LineUpTitle>예상 라인업</LineUpTitle>
        </LineTitleBox>

        <OverFlowX>
          <LineContentsContainer>
            {/* 홈 */}
            <LineUpTeamBox>
              <LineThead>
                <LineUpLogo url={Team}></LineUpLogo>
                <LineUpText name>레알마드리드</LineUpText>
              </LineThead>
              <LineThead>
                <LineUpText stapline style={{ width: 30 }}>
                  국가
                </LineUpText>
                <LineUpText stapline style={{ width: 150 }}>
                  이름
                </LineUpText>
                <LineUpText stapline style={{ width: 80 }}>
                  레이팅
                </LineUpText>
                <LineUpText stapline>번호</LineUpText>
                <LineUpText></LineUpText>
              </LineThead>

              {data?.map((el, idx) => {
                return (
                  <>
                    <LineTbody>
                      <LineUpText style={{ width: 30 }}>
                        <LineUpLogo country url={Country}></LineUpLogo>
                      </LineUpText>
                      <LineUpText style={{ width: 150 }}>
                        Ramon Pasquel (GK)
                      </LineUpText>
                      <LineUpText style={{ width: 80 }}>11/20</LineUpText>
                      <LineUpText style={{ width: 90 }}>41</LineUpText>

                      <LineUpText>
                        <Injury>부상</Injury>
                      </LineUpText>
                    </LineTbody>
                  </>
                );
              })}
            </LineUpTeamBox>

            {/* 어웨이 */}
            <LineUpTeamBox>
              <LineThead>
                <LineUpLogo url={Team}></LineUpLogo>
                <LineUpText name>레알마드리드</LineUpText>
              </LineThead>
              <LineThead>
                <LineUpText stapline style={{ width: 30 }}>
                  국가
                </LineUpText>
                <LineUpText stapline style={{ width: 150 }}>
                  이름
                </LineUpText>
                <LineUpText stapline style={{ width: 80 }}>
                  레이팅
                </LineUpText>
                <LineUpText stapline>번호</LineUpText>
                <LineUpText></LineUpText>
              </LineThead>
              {data?.map((el, idx) => {
                return (
                  <>
                    <LineTbody>
                      <LineUpText style={{ width: 30 }}>
                        <LineUpLogo country url={Country}></LineUpLogo>
                      </LineUpText>
                      <LineUpText style={{ width: 150 }}>
                        Ramon Pasquel (GK)
                      </LineUpText>
                      <LineUpText style={{ width: 80 }}>11/20</LineUpText>
                      <LineUpText style={{ width: 90 }}>41</LineUpText>

                      <LineUpText>
                        <Injury>부상</Injury>
                      </LineUpText>
                    </LineTbody>
                  </>
                );
              })}
            </LineUpTeamBox>
          </LineContentsContainer>
        </OverFlowX>

        <OverFlow>
          <SquadImg src={LineUp}></SquadImg>
        </OverFlow>
      </LineUpBox>
    </>
  );
};

export default LineUpModal;

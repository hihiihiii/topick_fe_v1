import styled from "@emotion/styled";
import { Link } from "react-router-dom";

const breakpoints = [470, 768, 992, 1200, 1700]; // 0 1 번쨰사용
const mq = breakpoints.map((bp) => `@media (max-width: ${bp}px)`);

const HeaderContainer = styled.div`
  width: 100%;
  background-color: #f2e5ff;
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
  box-sizing: border-box;
  padding: 0px 150px 0px 80px;
  @media (max-width: 1024px) {
    padding: 0px 40px 0px 0px;
  }
  ${mq[1]} {
    padding: 0px 40px 0px 0px;
  }
`;

const HeaderBox = styled.div`
  background-color: #f2e5ff;
  width: 100%;
  height: 86px;
  display: flex;
  justify-content: space-between;
`;
const HeaderBoxright = styled.div`
  display: flex;
  align-items: center;
  width: 100px;
  @media (max-width: 1024px) {
    display: none;
  }
  ${mq[1]} {
    display: none;
  }
`;

const HeaderBox_Text = styled.div`
  font-size: 18px;
  width: 125px;
  text-align: center;
  height: 30px;
  font-weight: 600;
  display: flex;
  font-weight: bold;
  flex-direction: row;
  align-items: center;
  justify-content: center;
  margin-top: 3px;
`;

const HeaderBoxleft = styled.div`
  color: #b975fd;
  font-size: 24px;
  height: 30px;

  display: flex;
  align-items: center;
  justify-content: center;
  margin-bottom: ${(props) => {
    if (props.mobile) {
      return "15px";
    }
  }};
`;

const HeaderNavigation = styled(Link)`
  text-decoration: none;
  color: ${(props) => {
    if (props.action) {
      return "#000";
    } else {
      return "#b068f8";
    }
  }};
  :hover {
    color: #000;
  }
`;

const HeaderImgFlexBox = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: center;
`;

const HeaderImg = styled.img`
  width: ${(props) => {
    if (props.mobile) {
      return "24px";
    }
    return "36px";
  }};
  height: 36px;
  object-fit: contain;
`;

const HeaderMiddleBox = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;

  ${mq[4]} {
    overflow-x: scroll;
    width: 100%;
    margin: 0px 30px;
    /* ::-webkit-scrollbar {
      display: none;
    } */
    /* ::-webkit-scrollbar-thumb {
      background-color: #2f3542;
    } */
  }

  @media (max-width: 1024px) {
    display: none;
  }
`;
const MenuBar = styled.div`
  display: none;
  ${mq[0]} {
    width: 32px;
    height: 32px;
    background-image: ${(props) => {
      if (props.url) {
        return `url(${props.url})`;
      }
    }};
  }
`;

const MobileMenu = styled.div`
  display: none;
  cursor: pointer;
  @media (max-width: 1024px) {
    display: block;
  }
  ${mq[1]} {
    display: block;
  }
`;

const ProfileName = styled.div`
  font-size: 16px;
  font-weight: 600;
  color: #000;
`;

const WebProfileFlex = styled(Link)`
  display: flex;
  align-items: center;
  justify-content: space-between;
  text-decoration: none;
  width: 100px;
`;

const NonLoginBox = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
  width: 125px;
  ${mq[1]} {
    display: none;
  }
`;

const NonLoginBar = styled.div`
  width: 2.2px;
  height: 20px;
  background-color: #b068f8;
`;

const NonLoginText = styled.div`
  font-size: 16px;
  font-weight: 600;
  color: #b068f8;
  :hover {
    color: #000;
  }
`;

export {
  NonLoginBox,
  NonLoginBar,
  NonLoginText,
  WebProfileFlex,
  ProfileName,
  MobileMenu,
  MenuBar,
  HeaderImgFlexBox,
  HeaderMiddleBox,
  HeaderBox,
  HeaderBoxright,
  HeaderBoxleft,
  HeaderBox_Text,
  HeaderContainer,
  HeaderNavigation,
  HeaderImg,
};

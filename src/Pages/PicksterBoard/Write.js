import React from "react";
import {
  BoardBox,
  BoardFlexEnd,
  BoardInputBox,
  BoardMarginTop,
  BoardText,
  BoardTextArea,
  BoardWrapper,
  CompletionBtn,
  Test,
} from "../../Styles/Board/Board";
import { Linked } from "../../Styles/Side";

const BoardWrite = ({ history }) => {
  return (
    <>
      <BoardWrapper>
        <BoardBox>
          <BoardText>픽스터 게시판 작성하기</BoardText>
          <BoardMarginTop>
            <BoardText strap>제목</BoardText>
            <BoardInputBox placeholder="제목을 입력해주세요"></BoardInputBox>
          </BoardMarginTop>
          <BoardMarginTop>
            <BoardText strap>내용</BoardText>
            <BoardTextArea placeholder="내용을 입력해주세요..."></BoardTextArea>
          </BoardMarginTop>
          <BoardFlexEnd>
            <CompletionBtn
              onClick={() => {
                window.scroll(0, 0);
                history.goBack();
              }}
            >
              완료
            </CompletionBtn>
          </BoardFlexEnd>
        </BoardBox>
      </BoardWrapper>
    </>
  );
};

export default BoardWrite;

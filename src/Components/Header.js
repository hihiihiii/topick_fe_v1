import React, { useState } from "react";
import { withRouter } from "react-router";
import { Link } from "react-router-dom";
import { Logo_Main, MenuImg, Profile } from "../Assets/file";
import {
  HeaderBox,
  HeaderBoxleft,
  HeaderBoxright,
  HeaderBox_Text,
  HeaderContainer,
  HeaderImg,
  HeaderMiddleBox,
  HeaderNavigation,
  MobileMenu,
  NonLoginBar,
  NonLoginBox,
  NonLoginText,
  ProfileName,
  WebProfileFlex,
} from "../Styles/Header";
import { LogoImg } from "../Styles/Global";
import { Linked } from "../Styles/Side";
import CustomModal from "./Modal";
import MobileSide from "./MyPage/MobileSide";

const Header = ({ location }) => {
  const [isOpen, setIsOpen] = useState(false);
  const Menu = [
    {
      title: "픽스터 Pick",
      link: "/pick",
    },
    {
      title: "공지사항",
      link: "/board",
    },
    {
      title: "픽스터 랭킹",
      link: "/rank",
    },
    {
      title: "프로토",
      link: "/proto",
    },
    // {
    //   title: "2부 리그",
    //   link: "/seconddivision",
    // },
    {
      title: "국내 구매율",
      link: "/purchaserate",
    },
    {
      title: "커버스픽",
      link: "/covers",
    },
    {
      title: "오늘의 팩트체크",
      link: "/todayfact",
    },
    {
      title: "라인업",
      link: "/lineup",
    },
    {
      title: "배당 하락율",
      link: "/dividenddroprate",
    },
  ];

  if (location.pathname === "/login" || location.pathname === "/register") {
    return <></>;
  } else {
    return (
      <>
        <CustomModal
          isOpen={isOpen}
          setIsOpen={setIsOpen}
          type="menu"
          Content={() => <MobileSide setIsOpen={setIsOpen} />}
        ></CustomModal>
        <HeaderBox>
          <HeaderContainer>
            <Link to="/" style={{ textDecoration: "none" }}>
              <HeaderBoxleft>
                <LogoImg main src={Logo_Main} />
              </HeaderBoxleft>
            </Link>
            <HeaderMiddleBox>
              {Menu?.map((el, idx) => {
                let action;
                if (location.pathname.indexOf(el.link) !== -1) {
                  action = true;
                } else {
                  action = false;
                }
                return (
                  <>
                    <HeaderNavigation to={el?.link} action={action}>
                      <HeaderBox_Text>{el?.title}</HeaderBox_Text>
                    </HeaderNavigation>
                  </>
                );
              })}
            </HeaderMiddleBox>

            {/* 비로그인 시 */}
            {/* <NonLoginBox>
              <Linked to="/login" more>
                <NonLoginText>로그인</NonLoginText>
              </Linked>
              <NonLoginBar></NonLoginBar>
              <Linked to="/register" more>
                <NonLoginText>회원가입</NonLoginText>
              </Linked>
            </NonLoginBox> */}

            {/* 로그인 시 */}
            <HeaderBoxright>
              <WebProfileFlex to="/profile/myprofile">
                <HeaderImg src={Profile}></HeaderImg>
                <ProfileName>hihihihi</ProfileName>
              </WebProfileFlex>
            </HeaderBoxright>

            <MobileMenu
              onClick={() => {
                setIsOpen(true);
              }}
            >
              <HeaderImg mobile src={MenuImg}></HeaderImg>
            </MobileMenu>
          </HeaderContainer>
        </HeaderBox>
      </>
    );
  }
};

export default withRouter(Header);

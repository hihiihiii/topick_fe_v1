import styled from "@emotion/styled";

const ProtoStateBox = styled.div`
  width: 51px;
  height: 28px;
  border-radius: 4px;
  background-color: ${(props) => {
    if (props.red) {
      return "#ffdede";
    }
    if (props.blue) {
      return "#dee7ff";
    }
    if (props.black) {
      return "#ededed";
    }
  }};

  color: ${(props) => {
    if (props.red) {
      return "#f85454";
    }
    if (props.blue) {
      return "#4171f2";
    }
    if (props.black) {
      return "#000";
    }
  }};
  display: flex;
  align-items: center;
  justify-content: center;
  font-weight: 600;
`;

const ProtoImg = styled.img`
  width: 21px;
  height: 29px;
  object-fit: contain;
`;

const ProtoTeamName = styled.div`
  font-size: 18px;
  font-weight: 600;
  color: #000;
`;

const ProtoFlexBox = styled.div`
  width: 125px;

  display: flex;
  align-items: center;
  justify-content: space-between;
`;

export { ProtoStateBox, ProtoImg, ProtoTeamName, ProtoFlexBox };

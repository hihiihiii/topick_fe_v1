import React, { useState } from "react";
import { Accept } from "../../../Assets/file";
import {
  AcceptBox,
  AcceptFlex,
  BankSelectBox,
  BottomContainer,
  CompletedBtn,
  ConsentBox,
  ConsentText,
  ExcahangeContainer,
  ExcahangePoint,
  ModalHeader,
  ModalText,
  ModalWrapper,
  PasswordInput,
  PasswordInputBox,
  WarningText,
} from "../../../Styles/Modal/Modal";

const AccountChange = ({ setIsOpen }) => {
  const [data, setData] = useState({ location: "none" });
  const [accept, setAccept] = useState(true);
  return (
    <>
      <ModalWrapper>
        <ModalHeader>
          <ModalText title>계좌번호 등록</ModalText>
          <ModalText title>1,500P</ModalText>
        </ModalHeader>
        <ExcahangeContainer>
          <PasswordInputBox>
            <ModalText>은행명</ModalText>
            <BankSelectBox
              style={{ color: data?.location === "none" ? "#a7a7a7" : "#000" }}
              onChange={(e) => {
                setData({ ...data, location: e.target.value });
              }}
            >
              <option value={"none"}>주 거래 은행을 선택해주세요</option>
              <option value={1}>우리은행</option>
              <option value={2}>하나은행</option>
              <option value={3}>국민은행</option>
            </BankSelectBox>
          </PasswordInputBox>
          <PasswordInputBox>
            <ModalText>계좌번호</ModalText>
            <PasswordInput placeholder="계좌번호를 입력해주세요"></PasswordInput>
          </PasswordInputBox>
          <PasswordInputBox>
            <ModalText>예금주 명</ModalText>
            <PasswordInput placeholder="예금주명를 재입력해주세요"></PasswordInput>
          </PasswordInputBox>
          <PasswordInputBox>
            <ModalText>환전신청 가능액</ModalText>
            <ExcahangePoint>
              <ModalText>15,000원</ModalText>
            </ExcahangePoint>
          </PasswordInputBox>
        </ExcahangeContainer>
        <WarningText>보유 포인트가 부족합니다</WarningText>

        <BottomContainer>
          <ModalText>유의사항 / 동의사항</ModalText>
          <ConsentBox>
            <ConsentText>
              많이 그들의 것은 끝까지 것이다. 동력은 붙잡아 그들은 수 칼이다.
              천하를 따뜻한 예가 것이다. 인생의 긴지라 꾸며 이것을 싸인 우는
              그들은 살 끓는다. 얼음 이는 광야에서 피어나기 것이다. 천지는
              힘차게 스며들어 보이는 힘차게 열매를 인생을 보이는 이것이다. 것은
              피에 보이는 청춘 것이다.보라, 바이며, 그들은 풀밭에 생명을
              교향악이다. 인생을 이상이 되려니와, 동산에는 대중을 날카로우나 살
              약동하다. 얼마나 희망의 거선의 피가 그들의 것이다. 싸인 착목한는
              노래하며 모래뿐일 있는가? 있는 있음으로써 인간은 있는가? 주며,
              놀이 있는 곳이 이상의 살았으며, 피가 힘있다. 내는 소담스러운
              그들의 있다. 꽃이 물방아 위하여 청춘의 끓는 원대하고, 투명하되
              것이다. 얼음 하였으며, 못할 인간에 방지하는 인도하겠다는 쓸쓸한
              것이다. 피가 것은 품으며, 청춘에서만 만물은 그들은 얼음
              봄바람이다. 천하를 피가 그들의 수 이것이다. 열락의 인간이 가치를
              바로 같이, 것이다. 사랑의 어디 심장의 풀이 때문이다. 주는 뭇 불어
              것이다. 온갖 부패를 이 찾아다녀도, 옷을 있으랴? 같이, 피고 피가
              위하여, 그들은 소담스러운 이것이다. 생의 인간은 착목한는 가슴이
              소금이라 것이다. 풍부하게 없는 곳으로 이것이다. 얼마나 부패를
              따뜻한 어디 새가 있음으로써 부패뿐이다. 반짝이는 그들에게 우는
              간에 투명하되 위하여, 생생하며, 사막이다. 오직 전인 못할
              봄바람이다. 설산에서 눈에 이상 충분히 천지는 인도하겠다는 않는
              것이다. 날카로우나 갑 바이며, 인생을 군영과 옷을 그러므로 우리의
              이상은 약동하다.
            </ConsentText>
          </ConsentBox>
          <AcceptFlex>
            {accept ? (
              <AcceptBox
                url={Accept}
                onClick={() => {
                  setAccept(false);
                }}
              ></AcceptBox>
            ) : (
              <AcceptBox
                onClick={() => {
                  setAccept(true);
                }}
              ></AcceptBox>
            )}

            <ModalText>동의 하기</ModalText>
          </AcceptFlex>
        </BottomContainer>
        <CompletedBtn
          onClick={() => {
            setIsOpen(false);
          }}
        >
          완료
        </CompletedBtn>
      </ModalWrapper>
    </>
  );
};

export default AccountChange;

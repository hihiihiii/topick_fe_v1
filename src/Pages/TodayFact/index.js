import React, { useState } from "react";
import { Team } from "../../Assets/file";
import { OverFlowX } from "../../Styles/PicksterBoard";
import {
  TodayCard,
  TodayCardBox,
  TodayCardFlex,
  TodayCardTitle,
  TodayContent,
  TodayContentFlex,
  TodayContentText,
  TodayFlexBox,
  TodayProfileImg,
  TodayTeamIcon,
  TodayTitleBox,
  TodayTitleText,
  TodayVersusText,
  TodayWrapper,
} from "../../Styles/TodayFact";

const TodayFact = () => {
  const [data, setData] = useState([{}, {}, {}]);
  return (
    <>
      <TodayWrapper>
        <TodayFlexBox>
          {data?.map((el, idx) => {
            return (
              <>
                <TodayCardBox>
                  <TodayTitleBox>
                    <TodayContent>
                      <TodayTitleText>레알마드리드</TodayTitleText>
                      <TodayTeamIcon url={Team}></TodayTeamIcon>
                      <TodayVersusText>vs</TodayVersusText>
                      <TodayTeamIcon url={Team}></TodayTeamIcon>
                      <TodayTitleText>레알마드리드</TodayTitleText>
                    </TodayContent>
                  </TodayTitleBox>
                  <OverFlowX>
                    <TodayCardFlex>
                      <TodayCard>
                        <TodayContentFlex>
                          <TodayProfileImg></TodayProfileImg>
                          <TodayCardTitle>No Scores</TodayCardTitle>
                        </TodayContentFlex>
                        <TodayContentText>
                          Confiança haven’t scored a goal in all of their last 3
                          home league matches
                        </TodayContentText>
                      </TodayCard>
                      <TodayCard>
                        <TodayContentFlex>
                          <TodayProfileImg></TodayProfileImg>
                          <TodayCardTitle>No Scores</TodayCardTitle>
                        </TodayContentFlex>
                        <TodayContentText>
                          Confiança haven’t scored a goal in all of their last 3
                          home league matches
                        </TodayContentText>
                      </TodayCard>
                      <TodayCard>
                        <TodayContentFlex>
                          <TodayProfileImg></TodayProfileImg>
                          <TodayCardTitle>No Wins</TodayCardTitle>
                        </TodayContentFlex>
                        <TodayContentText>
                          Confiança haven’t scored a goal in all of their last 3
                          home league matches
                        </TodayContentText>
                      </TodayCard>
                      <TodayCard>
                        <TodayContentFlex>
                          <TodayProfileImg></TodayProfileImg>
                          <TodayCardTitle>Winless</TodayCardTitle>
                        </TodayContentFlex>
                        <TodayContentText>
                          Confiança haven’t scored a goal in all of their last 3
                          home league matches
                        </TodayContentText>
                      </TodayCard>
                    </TodayCardFlex>
                  </OverFlowX>
                </TodayCardBox>
              </>
            );
          })}
        </TodayFlexBox>
      </TodayWrapper>
    </>
  );
};

export default TodayFact;

import styled from "@emotion/styled";

const breakpoint = [450, 768, 1200, 1920];
const mq = breakpoint.map((bp) => `@media (max-width: ${bp}px)`);

const PurchaseContainer = styled.div`
  display: flex;
  flex-direction: column;
`;

const PurchaseBox = styled.div`
  width: ${(props) => {
    if (props.full) {
      return "100%";
    }
    return "647px";
  }};
  height: 70px;
  border-radius: 10px;
  background-color: #fff;
  display: flex;
  align-items: center;
  justify-content: space-between;

  ${mq[1]} {
    width: 634px;
  }
  ${mq[0]} {
    width: 100%;
  }
`;

const PurchaseText = styled.div`
  font-size: ${(props) => {
    if (props.date) {
      return "13px";
    }
    return "16px";
  }};
  font-weight: ${(props) => {
    if (props.date) {
      return "500";
    }
    return "600";
  }};
  color: ${(props) => {
    if (props.date) {
      return "#b7b7b7";
    }
    return "#000";
  }};

  ${mq[0]} {
    font-size: ${(props) => {
      if (props.date) {
        return "10px";
      }
      return "14px";
    }};
    font-weight: ${(props) => {
      if (props.date) {
        return "500";
      }
      return "600";
    }};
  }
`;

const PurchaseDay = styled.div`
  width: 51px;
  height: 28px;
  display: flex;
  align-items: center;
  justify-content: center;
  border-radius: 4px;
  background-color: #dee7ff;
  color: #4171f2;
  font-size: 12px;
  font-weight: 600;
  margin-left: 10px;
`;

const PurchaseFlex = styled.div`
  display: flex;
  flex-direction: column;
  align-items: ${(props) => {
    if (props.start) {
      return "flex-start";
    }
    return "flex-end";
  }};
`;

const PurchaseRow = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
`;

export {
  PurchaseRow,
  PurchaseContainer,
  PurchaseBox,
  PurchaseText,
  PurchaseDay,
  PurchaseFlex,
};

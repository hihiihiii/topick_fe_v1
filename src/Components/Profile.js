import React, { useState } from "react";
import ContactDetails from "./MyPage/ContactDetails";
import MyBlog from "./MyPage/MyBlog";
import MyProfile from "./MyPage/MyProfile";
import PayPickster from "./MyPage/PayPickster";
import Point from "./MyPage/Point";
import PurchaseHistroy from "./MyPage/PurchaseHistroy";
import SalesHistory from "./MyPage/SalesHistory";
import {
  SideWrrapper,
  SideSelectBox,
  SideContainer,
  SideBox,
  Linked,
} from "../Styles/Side";
import { withRouter } from "react-router-dom/cjs/react-router-dom.min";
//내정보
const Profile = ({ location }) => {
  const [isOpen, setIsOpen] = useState(false);
  const [type, setType] = useState(1); // 타입 받아서 보여줘야함 mobile

  if (
    location.pathname === "/profile/myprofile" ||
    location.pathname === "/profile/point" ||
    location.pathname === "/profile/myblog" ||
    location.pathname === "/profile/paypickster" ||
    location.pathname === "/profile/saleshistory" ||
    location.pathname === "/profile/purchasehistroy" ||
    location.pathname === "/profile/contactdetails"
  ) {
    return (
      <>
        <SideWrrapper>
          <SideContainer>
            <SideBox>
              <SideSelectBox title>프로필설정</SideSelectBox>
              <Linked to="/profile/myprofile">
                <SideSelectBox
                  action={type === 1 ? true : false}
                  onClick={() => {
                    setType(1);
                  }}
                >
                  내 프로필
                </SideSelectBox>
              </Linked>

              <Linked to="/profile/point">
                <SideSelectBox
                  action={type === 2 ? true : false}
                  onClick={() => {
                    setType(2);
                  }}
                >
                  포인트
                </SideSelectBox>
              </Linked>
              <Linked to="/profile/purchasehistroy">
                <SideSelectBox
                  action={type === 3 ? true : false}
                  onClick={() => {
                    setType(3);
                  }}
                >
                  구매내역
                </SideSelectBox>
              </Linked>
              <Linked to="/profile/saleshistory">
                <SideSelectBox
                  action={type === 4 ? true : false}
                  onClick={() => {
                    setType(4);
                  }}
                >
                  판매내역
                </SideSelectBox>
              </Linked>
              <Linked to="/profile/paypickster">
                <SideSelectBox
                  action={type === 5 ? true : false}
                  onClick={() => {
                    setType(5);
                  }}
                >
                  유료 픽스터
                </SideSelectBox>
              </Linked>
              <Linked to="/profile/contactdetails">
                <SideSelectBox
                  action={type === 6 ? true : false}
                  onClick={() => {
                    setType(6);
                  }}
                >
                  문의 내역
                </SideSelectBox>
              </Linked>

              <Linked to="/profile/myblog">
                <SideSelectBox
                  bottom
                  action={type === 7 ? true : false}
                  onClick={() => {
                    setType(7);
                  }}
                >
                  내 블로그
                </SideSelectBox>
              </Linked>
            </SideBox>
          </SideContainer>
        </SideWrrapper>
      </>
    );
  } else {
    return <></>;
  }
};

export default withRouter(Profile);

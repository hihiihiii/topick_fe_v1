import React, { useState } from "react";
import { Team } from "../../Assets/file";
import { OverFlowX } from "../../Styles/PicksterBoard";
import {
  ProtoFlexBox,
  ProtoImg,
  ProtoStateBox,
  ProtoTeamName,
} from "../../Styles/Proto";
import {
  PurchaseContainer,
  PurchaseTitle,
  PurchaseTop_tbody,
  PurchaseTop_td,
  PurchaseTop_th,
  PurchaseTop_thead,
  PurchaseWrapper,
  Purchase_Table,
  ResultBox,
  ResultText,
} from "../../Styles/PurchaseRate";
import CustomeModal from "../../Components/Modal";
import GameInfomation from "../../Components/Modal/Block/GameInfomation";
import TeamInfomation from "../../Components/Modal/Block/TeamInfomation";

const Proto = () => {
  const [data, setData] = useState([{}, {}]);
  const [gameOpen, setGameOpen] = useState(false);
  const [teamOpen, setTeamOpen] = useState(false);

  return (
    <>
      <CustomeModal
        isOpen={gameOpen}
        setIsOpen={setGameOpen}
        type="modal"
        Content={() => <GameInfomation setIsOpen={setGameOpen} />}
      />
      <CustomeModal
        isOpen={teamOpen}
        setIsOpen={setTeamOpen}
        Content={() => <TeamInfomation setIsOpen={setTeamOpen} />}
      />
      <PurchaseWrapper>
        <PurchaseContainer>
          <PurchaseTitle title>프로토</PurchaseTitle>
          <OverFlowX>
            <Purchase_Table>
              <PurchaseTop_thead>
                <PurchaseTop_th style={{ width: 125 }}>경기</PurchaseTop_th>
                <PurchaseTop_th style={{ width: 175 }}>시간</PurchaseTop_th>
                <PurchaseTop_th style={{ width: 255 }}>상태</PurchaseTop_th>
                <PurchaseTop_th style={{ width: 175 }}>홈팀</PurchaseTop_th>
                <PurchaseTop_th style={{ width: 95 }}>점수</PurchaseTop_th>
                <PurchaseTop_th style={{ width: 175 }}>원정팀</PurchaseTop_th>
                <PurchaseTop_th style={{ width: 100 }}>하프타임</PurchaseTop_th>
              </PurchaseTop_thead>
              {data?.map((el, idx) => {
                return (
                  <PurchaseTop_tbody>
                    <PurchaseTop_td
                      pointer
                      style={{ width: 125 }}
                      onClick={() => {
                        setGameOpen(true);
                      }}
                    >
                      JPND2
                    </PurchaseTop_td>
                    <PurchaseTop_td style={{ width: 175 }}>8:00</PurchaseTop_td>
                    <PurchaseTop_td style={{ width: 255 }}>
                      <ProtoStateBox red>취소</ProtoStateBox>
                    </PurchaseTop_td>

                    <PurchaseTop_td
                      pointer
                      style={{ width: 175 }}
                      onClick={() => {
                        setTeamOpen(true);
                      }}
                    >
                      <ProtoFlexBox>
                        <ProtoTeamName>레알마드리드</ProtoTeamName>
                        <ProtoImg src={Team}></ProtoImg>
                      </ProtoFlexBox>
                    </PurchaseTop_td>

                    <PurchaseTop_td style={{ width: 95 }}>3:2</PurchaseTop_td>
                    <PurchaseTop_td
                      style={{ width: 175 }}
                      pointer
                      onClick={() => {
                        setTeamOpen(true);
                      }}
                    >
                      <ProtoFlexBox>
                        <ProtoImg src={Team}></ProtoImg>
                        <ProtoTeamName>레알마드리드</ProtoTeamName>
                      </ProtoFlexBox>
                    </PurchaseTop_td>
                    <PurchaseTop_td style={{ width: 100 }}>1-0</PurchaseTop_td>
                  </PurchaseTop_tbody>
                );
              })}
            </Purchase_Table>
          </OverFlowX>
        </PurchaseContainer>
      </PurchaseWrapper>
    </>
  );
};

export default Proto;

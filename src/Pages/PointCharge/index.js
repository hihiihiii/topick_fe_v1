import React from "react";
import {
  PointBox,
  PointBoxCenter,
  PointColumn,
  PointContainer,
  PointHeader,
  PointPay,
  PointText,
  PointWrapBox,
  PointWrapper,
} from "../../Styles/PointCharge";
import { Linked } from "../../Styles/Side";

const PointCharge = ({ history }) => {
  return (
    <>
      <PointWrapper>
        <PointContainer>
          <PointHeader>
            <PointText>포인트 충전</PointText>
            <PointColumn>
              <PointText sub>보유 포인트</PointText>
              <PointText point>15,000 P</PointText>
            </PointColumn>
          </PointHeader>

          <PointWrapBox>
            <PointBox></PointBox>
            <PointBox></PointBox>
            <PointBox></PointBox>
            <PointBox></PointBox>
            <PointBox></PointBox>
            <PointBox></PointBox>
            <PointBox></PointBox>
            <PointBox></PointBox>
            <PointBox></PointBox>
          </PointWrapBox>

          <PointBoxCenter>
            <PointPay
              onClick={() => {
                history.goBack();
              }}
            >
              결제하기
            </PointPay>
          </PointBoxCenter>
        </PointContainer>
      </PointWrapper>
    </>
  );
};

export default PointCharge;

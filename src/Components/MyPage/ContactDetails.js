import React, { useState } from "react";
import { PointBox, PointImg } from "../../Styles/Mypage/Point";
import {
  Inner,
  ProfileHeader,
  ProfileTitle,
} from "../../Styles/Mypage/Profile";
import {
  PurchaseBox,
  PurchaseContainer,
  PurchaseFlex,
  PurchaseRow,
  PurchaseText,
} from "../../Styles/Mypage/Purchase";
import { Linked } from "../../Styles/Side";

const ContactDetails = () => {
  const [data, setData] = useState([{}, {}, {}]);
  return (
    <Inner>
      <ProfileHeader>
        <ProfileTitle>문의내역</ProfileTitle>
        <Linked more to="/contact/write">
          <PointBox
            action
            // action={type === 1 ? true : false}
            // onClick={() => {
            //   setType(1);
            // }}
          >
            문의하기
          </PointBox>
        </Linked>
      </ProfileHeader>
      <PurchaseContainer>
        {data?.map((el) => {
          return (
            <>
              <Linked to="/contact/detail">
                <PurchaseBox full>
                  <PurchaseRow>
                    <PointImg history></PointImg>
                    <PurchaseFlex start>
                      <PurchaseText>문의 내역입니다.</PurchaseText>
                      <PurchaseText date>dlduddls님</PurchaseText>
                    </PurchaseFlex>
                  </PurchaseRow>
                  <PurchaseFlex>
                    <PurchaseText date>2021.09.01 14:21</PurchaseText>
                  </PurchaseFlex>
                </PurchaseBox>
              </Linked>
            </>
          );
        })}
      </PurchaseContainer>
    </Inner>
  );
};

export default ContactDetails;

import styled from "@emotion/styled";

const breakpoints = [450, 768, 1200, 1400, 420];
const mq = breakpoints.map((bp) => `@media (max-width:${bp}px)`);

const InfoWrapper = styled.div`
  width: 1331px;
  height: 100%;
  border-radius: 5px;
  background-color: #fff;

  ${mq[1]} {
    width: 531px;
    height: 867.7px;
  }
  ${mq[0]} {
    width: 375px;
    height: 763.7px;
  }
  ${mq[4]} {
    width: 321px;
  }
`;

const InfoText = styled.div`
  font-size: ${(props) => {
    if (props.title) {
      return "22px";
    }
    return "16px";
  }};
  font-weight: 600;
  color: #000;
  margin-top: ${(props) => {
    if (props.title) {
      return "0px";
    }
    return "45px";
  }};
  ${mq[0]} {
    font-size: ${(props) => {
      if (props.title) {
        return "16px";
      }
    }};
  }
`;

const CardFlexBox = styled.div`
  width: 305px;
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
`;

const CardBox = styled.div`
  width: 65px;
  height: 73px;
  border-radius: 5px;
  border: solid 1px #f8f8f8;
  background-color: #f8f8f8;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  margin-top: 35.6px;
  cursor: pointer;
  box-shadow: ${(props) => {
    if (props.action) return "0 3px 6px 0 rgba(0, 0, 0, 0.16)";
  }};
`;

const CardText = styled.div`
  font-size: ${(props) => {
    if (props.sub) {
      return "12px";
    }
    return "22px";
  }};
  font-weight: 500;
  color: ${(props) => {
    if (props.sub) {
      return "#acacac";
    }
    return "#000";
  }};
`;

const InfoThead = styled.div`
  width: 100%;
  height: 57px;
  display: flex;
  align-items: center;
  border-radius: 10px;
  padding-left: 50px;
  box-sizing: border-box;
  background-color: #f8f8f8;
`;

const InfoTbody = styled.div`
  width: 100%;
  height: 60px;
  background-color: #fff;
  display: flex;
  align-items: center;
  padding-left: 50px;
  box-sizing: border-box;
  border-bottom: 1px solid #f8f8f8;
`;

const InfoTableText = styled.div`
  font-size: ${(props) => {
    if (props.th) {
      return "13px";
    }
    return "14px";
  }};
  font-weight: 600;
  color: ${(props) => {
    if (props.th) {
      return "#888";
    }
    return "#000";
  }};
  cursor: ${(props) => {
    if (props.pointer) {
      return "pointer";
    }
  }};
`;

const InfoTable = styled.div`
  display: flex;
  flex-direction: column;
  margin-top: 30.2px;
  margin-bottom: 65px;

  ${mq[1]} {
    width: fit-content;
  }
`;

const OddsBox = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  width: 51px;
  height: 28px;
  border-radius: 4px;
  background-color: #ffdede;
  font-size: 12px;
  font-weight: 600;
  color: #f85454;
`;

const RelatedArticlesFlex = styled.div`
  width: fit-content;
  display: flex;
  align-items: center;
  margin-top: 22px;
  margin-bottom: 22px;
`;

const RelatedArticlesBox = styled.div`
  width: 368px;
  height: 131px;
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  background-color: #f8f8f8;
  padding: 15px;
  box-sizing: border-box;
  margin-right: 20px;
`;

const RelatedFlex = styled.div`
  display: flex;
  flex-direction: row;

  justify-content: space-between;
`;

const RelatedText = styled.div`
  font-size: ${(props) => {
    if (props.date) {
      return "14px";
    }
    if (props.content) {
      return "16px";
    }
    return "22px";
  }};
  font-weight: 600;
  color: ${(props) => {
    if (props.date) {
      return "#bebebe";
    }
    return "#000";
  }}; ;
`;

const RelatedOverFlow = styled.div`
  width: 100%;
  overflow-x: scroll;
  ::-webkit-scrollbar {
    display: none;
  }
`;

const GameTitleBox = styled.div`
  width: 100%;
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
`;

const CancleBtn = styled.div`
  width: 30px;
  height: 30px;
  background-color: #f8f8f8;
  border-radius: 999px;
  cursor: pointer;
`;

const BoldText = styled.span`
  color: ${(props) => {
    if (props.bold) {
      return "#ff6969";
    }
    return "#000";
  }};
  font-size: 14px;
  font-weight: bold;
`;

const GameText = styled.div`
  font-size: 14px;
  font-weight: 500;
  color: #000;
`;

const GameTextFlex = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  margin-top: 16.5px;
  width: 257px;
`;

export {
  GameText,
  GameTextFlex,
  BoldText,
  CancleBtn,
  GameTitleBox,
  RelatedOverFlow,
  RelatedText,
  RelatedFlex,
  RelatedArticlesFlex,
  RelatedArticlesBox,
  OddsBox,
  InfoWrapper,
  InfoText,
  InfoTable,
  CardBox,
  CardText,
  CardFlexBox,
  InfoThead,
  InfoTbody,
  InfoTableText,
};

import React from "react";
import Modal from "react-modal";

const CustomModal = ({ isOpen, setIsOpen, Content, type }) => {
  const customStyles = {
    content: {
      top: "50%",
      left: "50%",
      right: "auto",
      bottom: "auto",
      marginRight: "-50%",
      transform: "translate(-50%, -50%)",
      borderRadius: "10px",
    },
  };

  const menuStyles = {
    content: {
      width: "75%",
      height: "100%",
      top: "50%",
      left: "42.5%",
      right: "auto",
      bottom: "auto",
      padding: "0px 5%",
      transform: "translate(-50%, -50%)",
      translateX: "50%",
    },
  };

  // const modalStyles = {
  //   content: {
  //     top: "50%",
  //     left: "50%",
  //     right: "auto",
  //     bottom: "auto",
  //     marginRight: "-50%",
  //     transform: "translate(-50%, -50%)",
  //     borderRadius: "10px",
  //   },
  // };

  return (
    <>
      <Modal
        isOpen={isOpen}
        onRequestClose={() => setIsOpen(false)}
        style={type === "menu" ? menuStyles : customStyles}
      >
        <Content isOpen={isOpen} setIsOpen={setIsOpen} />
      </Modal>
    </>
  );
};

export default CustomModal;

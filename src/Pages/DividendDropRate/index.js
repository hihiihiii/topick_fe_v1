import React, { useState, useEffect } from "react";
import { ArrowDown, ArrowUp, Line, RightArrow, Team } from "../../Assets/file";
import { CoversFlexBox, CoversImg, CoversText } from "../../Styles/Covers";
import {
  DividenedFlexBox,
  DropBox,
  DropFlex,
  DropImg,
  DropText,
  DropTimeBox,
} from "../../Styles/Dividend";
import { OverFlowX } from "../../Styles/PicksterBoard";
import {
  PurchaseContainer,
  PurchaseTitle,
  PurchaseTop_tbody,
  PurchaseTop_td,
  PurchaseTop_th,
  PurchaseTop_thead,
  PurchaseWrapper,
  Purchase_Table,
} from "../../Styles/PurchaseRate";

const DividendDropRate = () => {
  const [data, setData] = useState([{}, {}]);

  const [seconds, setSeconds] = useState(parseInt(60));

  useEffect(() => {
    const countdown = setInterval(() => {
      if (parseInt(seconds) > 0) {
        setSeconds(parseInt(seconds) - 1);
      }
      if (parseInt(seconds) === 0) {
        setSeconds(60);
      }
    }, 1000);
    return () => clearInterval(countdown);
  }, [seconds]);

  // const _handleCount = () => {
  //   let count = setInterval(60000);
  //   return count;
  // };
  return (
    <>
      <PurchaseWrapper>
        <PurchaseContainer>
          <DividenedFlexBox>
            <PurchaseTitle>배당 하락율</PurchaseTitle>
            <DropTimeBox
              onClick={() => {
                setSeconds(60);
              }}
            >
              {seconds} 초 뒤에 업데이트
            </DropTimeBox>
          </DividenedFlexBox>
          <OverFlowX>
            <Purchase_Table>
              <PurchaseTop_thead>
                <PurchaseTop_th style={{ width: 125 }}>경기번호</PurchaseTop_th>
                <PurchaseTop_th style={{ width: 175 }}>시간</PurchaseTop_th>
                <PurchaseTop_th style={{ width: 325 }}>
                  홈팀 vs 원정팀
                </PurchaseTop_th>
                <PurchaseTop_th style={{ width: 175 }}>1</PurchaseTop_th>
                <PurchaseTop_th style={{ width: 175 }}>X</PurchaseTop_th>
                <PurchaseTop_th style={{ width: 142.5 }}>2</PurchaseTop_th>
                <PurchaseTop_th style={{ width: 85 }}>B</PurchaseTop_th>
              </PurchaseTop_thead>
              {data?.map((el, idx) => {
                return (
                  <PurchaseTop_tbody covers>
                    <PurchaseTop_td style={{ width: 125 }}>
                      JDKFG09
                    </PurchaseTop_td>
                    <PurchaseTop_td style={{ width: 175 }}>
                      2021.12.02
                    </PurchaseTop_td>
                    <PurchaseTop_td style={{ width: 265 }}>
                      레알 마드리드 vs 레알 마드리드
                    </PurchaseTop_td>
                    <PurchaseTop_td style={{ width: 175 }}>
                      <DropFlex row>
                        <DropBox red>
                          <DropFlex>
                            <DropFlex row>
                              <DropText>1,500</DropText>
                              <DropImg mini src={RightArrow}></DropImg>
                              <DropText>220,000</DropText>
                            </DropFlex>
                            <DropText bottom>7/41 (35%)</DropText>
                          </DropFlex>
                        </DropBox>
                        <DropImg src={ArrowDown}></DropImg>
                      </DropFlex>
                    </PurchaseTop_td>
                    <PurchaseTop_td style={{ width: 175 }}>
                      <DropFlex row>
                        <DropBox blue>
                          <DropFlex>
                            <DropFlex row>
                              <DropText>1,500</DropText>
                              <DropImg mini src={RightArrow}></DropImg>
                              <DropText>220,000</DropText>
                            </DropFlex>
                            <DropText bottom>7/41 (35%)</DropText>
                          </DropFlex>
                        </DropBox>
                        <DropImg src={ArrowUp}></DropImg>
                      </DropFlex>
                    </PurchaseTop_td>
                    <PurchaseTop_td style={{ width: 200 }}>
                      <DropFlex row>
                        <DropBox black>
                          <DropFlex>
                            <DropFlex row>
                              <DropText>1,500</DropText>
                              <DropImg mini src={RightArrow}></DropImg>
                              <DropText>220,000</DropText>
                            </DropFlex>
                            <DropText bottom>7/41 (35%)</DropText>
                          </DropFlex>
                        </DropBox>
                        <DropImg mini src={Line}></DropImg>
                      </DropFlex>
                    </PurchaseTop_td>
                    <PurchaseTop_td style={{ width: 85 }}>12</PurchaseTop_td>
                  </PurchaseTop_tbody>
                );
              })}
            </Purchase_Table>
          </OverFlowX>
        </PurchaseContainer>
      </PurchaseWrapper>
    </>
  );
};

export default DividendDropRate;
